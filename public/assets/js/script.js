//conditional statements are statments that help our program decide what to do

/*There are 3 main types of conditional statements
1. if-else
2. switch
3. try-catch-finally

*/

//If-else statements are used to make declaration based on a condition
let age = 10;

if (age >= 18) {
	console.log("You may but wine");
} else {
	console.log("You can not buy wine");
}

/*
Syntax:
if (condition){
	do this if the condition are true
} else {
	do this if the condition are false
}
*/

//Multiple conditions

let age2 = 19;
let isFullTime = false

if (age2 >= 18 && !isFullTime) {
	console.log("Can enroll to bootcamp day class");
} else {

}	console.log("Can enroll to bootcamp night class");

//If else chaining - this is done to combine several possibble outcomes
/*
If money is greater than 500 - take a grab
If money is between 100 10 500 - take a taxi
If money is between 50 to 100 - take a jeep
If money is between 1 to 50 - walk
*/

let paoMoney = 100;
if (paoMoney > 500) {
	console.log("Will ride a grab");
} else if (paoMoney > 100 && paoMoney <= 500) {
	console.log("Will ride a taxi");
} else if (paoMoney > 50 && paoMoney <= 100) {
	console.log("Will ride a jeep");
} else if (paoMoney > 1 && paoMoney <= 50) {
	console.log("Will walk");
} else {
	console.log("Stay home")
}